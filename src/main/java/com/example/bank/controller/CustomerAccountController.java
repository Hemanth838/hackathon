package com.example.bank.controller;

import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.bank.config.ConfigurationManager;
import com.example.bank.dto.AddAccountRequestDto;
import com.example.bank.dto.FavouriteRecordsResponseDto;
import com.example.bank.dto.ResponseDto;
import com.example.bank.exception.CustomerNotFound;
import com.example.bank.exception.InvalidAccountExceptionHandler;
import com.example.bank.exception.InvalidUserIdExceptionHandler;
import com.example.bank.exception.UnauthorizedException;
import com.example.bank.service.CustomerAccountService;
import com.example.bank.service.CustomerService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/customers")
public class CustomerAccountController {

	Logger logger = LoggerFactory.getLogger(CustomerAccountController.class);

	@Autowired
	CustomerAccountService custAccountService;

	@Autowired
	CustomerService customerService;

	@Autowired
	ConfigurationManager config;

	
	ConfigurationManager configurationManager;

	/**
	 * This method delete an account from a favourite accounts of a customers
	 * 
	 * @param customerId
	 * @param iban
	 * @return ResponseDto
	 * @throws InvalidUserIdExceptionHandler
	 * @throws InvalidAccountExceptionHandler
	 * @author rakeshr.ra
	 */

	@ApiResponses(value = { @ApiResponse(code = 4001, message = "invalid customer id"),
			@ApiResponse(code = 4011, message = "incorrect  iban"),
			@ApiResponse(code = 4011, message = "account is not in the favourite list"),
			@ApiResponse(code = 2002, message = "account is successfully deleted from favourite list") })
	@DeleteMapping("/customers/{customerId}/accounts/{iban}")
	public ResponseEntity<ResponseDto> deleteFavAccount(@PathVariable("customerId") Long customerId,
			@PathVariable String iban) throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler {

		return new ResponseEntity<>(custAccountService.deleteFavAccount(customerId, iban), HttpStatus.OK);
	}

	/**
	 * This method is for Fetching favourite accounts of the customer
	 * 
	 * @param loggedInUserId
	 * @param customerId
	 * @param pageNumber
	 * @param noOfRecordsPerPage
	 * @return BookingResponse
	 * @throws CustomerNotFound
	 * @throws UnauthorizedException
	 */

	@ApiOperation(value = "Favourite accounts")
	@ApiResponses(value = { @ApiResponse(code = 4008, message = "user is not authorized to perform operation"),
			@ApiResponse(code = 4042, message = "customer not found"),
			@ApiResponse(code = 4009, message = "max value of page number is ${pageNumber} ") })
	@GetMapping("/{customerId}/accounts")

	public ResponseEntity<List<FavouriteRecordsResponseDto>> getFavouriteAccounts(@RequestHeader Long loggedInUserId,
			@PathVariable Long customerId, @RequestParam(required=false, defaultValue = "0") Integer pageNumber,
			@RequestParam(required=false,defaultValue = "3") Integer noOfRecordsPerPage) {

		logger.info("CustomerAccountController : getFavouriteAccounts");

		return new ResponseEntity<>(
				customerService.getFavouriteAccounts(loggedInUserId, customerId, pageNumber, noOfRecordsPerPage),
				HttpStatus.OK);
	}

	/**
	 * This method adds an account as a favourite account for a customers
	 * 
	 * @param customerId
	 * @param request
	 * @return ResponseDto
	 */
	@ApiOperation(value = "Add an account as a favourite account")
	@ApiResponses(value = { @ApiResponse(code = 4006, message = "Account is already added to favorites list"),
			@ApiResponse(code = 4042, message = "Customer Not Found") })
	@PostMapping("/{customerId}/accounts")
	public ResponseEntity<ResponseDto> addFavAccount(@PathVariable("customerId") Long customerId,
			@Valid @RequestBody AddAccountRequestDto request) {
		logger.info("CustomerAccountController: Inside addFavAccount method");
		return new ResponseEntity<>(custAccountService.addFavouriteAccount(request, customerId), HttpStatus.OK);
	}

}
