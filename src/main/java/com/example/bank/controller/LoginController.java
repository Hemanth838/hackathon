package com.example.bank.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.bank.dto.LoginDto;
import com.example.bank.dto.ResponseDto;
import com.example.bank.exception.InvalidUserIdExceptionHandler;
import com.example.bank.service.LoginService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

/**
 * This Controller is used to authenticate the customer, whether customer is valid or not
 * @
 * @param loginDto
 * @return
 * @throws InvalidUserIdExceptionHandler
 * @author rakeshr.ra
 */
@RestController
@RequestMapping("/api")
@Slf4j
public class LoginController {
	@Autowired
	LoginService loginService;

	@ApiOperation(value ="login")
	@ApiResponses(value = { @ApiResponse(code = 4001, message = "no customer id found"),
			@ApiResponse(code = 2000, message = "login success") })
	@PostMapping("/login")
	public ResponseEntity<ResponseDto> authenticateCustomer(@Valid @RequestBody LoginDto loginDto)
			throws InvalidUserIdExceptionHandler {
		log.info("loginController authenticateCustomer:in controller layer ");
		return new ResponseEntity<>(loginService.authenticateCustomer(loginDto), HttpStatus.OK);

	}
}
