/**
 * 
 */
package com.example.bank.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

import lombok.Data;

/**
 * @author hemanth.garlapati
 *
 */
@Configuration
@ConfigurationProperties
@PropertySource("classpath:messages-en.properties")
@Data
public class ConfigurationManager {

	private String unauthorizedUser;

	String ibanMandatoryMsg;
	String ibanLengthMsg;
	String ibanFormatMsg;

	String customerIdisinvalid;
	String loginSuccess;
	String deleteSuccess;
	String accountNumberNotExist;
	String payeeNotExist;
	String loginFailed;
	String accountExistsMsg;
	String addAccountSuccessMsg;
	String customerNotFoundMsg;
	String maxAccountExceeded;
	String customerNotFound;
	String limitExceeded;
}
