package com.example.bank.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import com.example.bank.constant.Constants;

import lombok.Data;

@Component
@Data
public class AddAccountRequestDto {
	
	
	@NotNull(message =Constants.IBAN_MANDATORY)
	@NotEmpty(message =Constants.IBAN_NON_EMPTY)
	@Size(max=20 , message= Constants.IBAN_SIZE_EXCEEDS)
	@Pattern(regexp = Constants.IBAN_PATTERN ,message=Constants.IBAN_PATTERN_MISMATCH)
	String iban;

}
