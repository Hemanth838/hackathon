package com.example.bank.dto;

import javax.validation.constraints.NotEmpty;

import com.example.bank.constant.Constants;
import com.sun.istack.NotNull;

import lombok.Getter;
import lombok.Setter;

/**
 * @author rakeshr.ra
 *
 */
@Getter
@Setter
public class LoginDto {
	@NotNull
	@NotEmpty(message = Constants.CUSTOMER_ID_NON_EMPTY)
	Long customerId;

}
