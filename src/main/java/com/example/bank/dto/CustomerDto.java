/**
 * 
 */
package com.example.bank.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hemanth.garlapati
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CustomerDto {
	
	private Long customerId;
	private String customerName;
	private String customerEmail;
	private Boolean isActive;
	List<AccountDto> customerAccounts;

}
