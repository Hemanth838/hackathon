/**
 * 
 */
package com.example.bank.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class FavouriteRecordsResponseDto {
	
	private String accountOwner;
	private String iban;
	private String bankName;

}
