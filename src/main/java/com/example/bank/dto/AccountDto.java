package com.example.bank.dto;

import com.example.bank.entity.Bank;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author hemanth.garlapati
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AccountDto {

	Long accountId;
	String accountOwner;
	String iban;
	Bank bank;

}
