package com.example.bank.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
/**
 * @author rakeshr.ra
 *
 */
@Entity
@Getter
@Setter
@AllArgsConstructor

@NoArgsConstructor
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	Long accountId;
	String accountOwner;
	@Column(unique=true)
	String iban;
	
	@ManyToOne
	@JoinColumn(name="bank_id")
    Bank bankId;
    
    
   
    
}
