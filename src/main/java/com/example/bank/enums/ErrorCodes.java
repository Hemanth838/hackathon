package com.example.bank.enums;

/**
 * @author rakeshr.ra
 *
 */
public enum ErrorCodes {
	VALIDATION_FAILED(4001l), LOGIN_SUCCESS(2000L), INVALID_ID(4001L), 
	UNAUTHORIZED_USER(4008l),DELETE_SUCCESS(2002l),INVALID_ACCOUNT_NO(4011L), INVALID_ID_LOGIN(4001L),CUSTOMER_NOT_FOUND(4042l),
	PAGE_LIMIT_EXCEEDED(4009l);
	
	Long errorCode;

	ErrorCodes(Long value) {
		this.errorCode = value;
	}

	public Long getErrorCode() {
		return errorCode;
	}

}