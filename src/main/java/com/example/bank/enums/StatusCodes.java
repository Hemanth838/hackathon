package com.example.bank.enums;
/**
 * @author rakeshr.ra
 *
 */
public enum StatusCodes {
	VALIDATION_FAILED(4001l), ACCOUNT_ADDED(2010l), DUPLICATE_ENTRY(4006l), CUSTOMER_NOT_FOUND(4042l), MAXIMUM_FAV_ACCOUNTS_EXCEEDED(4007l),
	LOGIN_SUCCESS(2000L), INVALID_ID(4001L),PAGE_LIMIT_EXCEEDED(4009l), 
	UNAUTHORIZED_USER(4008l),DELETE_SUCCESS(2002l),INVALID_ACCOUNT_NO(4011L);

	Long errorCode;

	StatusCodes(Long value) {
		this.errorCode = value;
	}

	public Long getErrorCode() {
		return errorCode;
	}

}