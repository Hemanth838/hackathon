package com.example.bank.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.bank.entity.Bank;

@Repository
public interface BankRepository  extends JpaRepository<Bank, Long>{

	Optional<Bank> findByBankName(String bankName);


}
