package com.example.bank.constant;

public class Constants {

	private Constants() {

	}

	public static final String BASE_URL = "http://{hostName}:9080/";
	public static final String HOSTNAME = "localhost";
	public static final String BANK_API_URL = "api/bankinfo/{iban}";
	public static final String SLASH = "/";

	public static final String IBAN_MANDATORY = "Iban is Mandatory";
	public static final String IBAN_SIZE_EXCEEDS = "Max 20 characters are allowed for IBAN";
	public static final String IBAN_PATTERN_MISMATCH = "IBAN is not in proper format - Should contain only letters and numbers";

	public static final String IBAN_PATTERN = "^[A-Za-z0-9]*$";
	public static final String IBAN_NON_EMPTY = "Iban Must Not be Empty";
	public static final String CUSTOMER_ID="customer id Must Not be Empty";
	public static final String CUSTOMER_ID_NON_EMPTY = "customer id Must Not be Empty";

}
