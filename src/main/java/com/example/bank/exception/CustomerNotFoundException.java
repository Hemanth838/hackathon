package com.example.bank.exception;

public class CustomerNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CustomerNotFoundException(String arg0) {
		super(arg0);
	}
	
	

}
