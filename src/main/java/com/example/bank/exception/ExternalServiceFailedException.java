package com.example.bank.exception;

public class ExternalServiceFailedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ExternalServiceFailedException(String arg0) {
		super(arg0);
	}
	
	

}
