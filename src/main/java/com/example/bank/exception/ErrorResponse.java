package com.example.bank.exception;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author rakeshr.ra
 *
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ErrorResponse {
	Long errorCode;
	List<String> errorMessage;

}
