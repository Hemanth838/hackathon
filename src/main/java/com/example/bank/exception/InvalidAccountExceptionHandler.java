package com.example.bank.exception;

public class InvalidAccountExceptionHandler extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidAccountExceptionHandler(String message) {
		super(message);
		
	}
	

}
