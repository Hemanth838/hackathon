/**
 * 
 */
package com.example.bank.exception;

/**
 * @author hemanth.garlapati
 *
 * 
 */
public class PageLimitExceededException extends RuntimeException {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PageLimitExceededException(String message) {
		super(message);
		
	}

}
