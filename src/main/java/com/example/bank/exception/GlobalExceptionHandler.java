package com.example.bank.exception;

import java.util.ArrayList;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.example.bank.enums.StatusCodes;

/**
 * @author rakeshr.ra
 *
 */
@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {

	@Override
	protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {
		List<String> details = new ArrayList<>();
		for (ObjectError error : ex.getBindingResult().getAllErrors()) {
			details.add(error.getDefaultMessage());
		}
		ErrorResponse error = new ErrorResponse(StatusCodes.VALIDATION_FAILED.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(DuplicateEntryException.class)
	public final ResponseEntity<Object> handleDupliscateEntryException(DuplicateEntryException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(StatusCodes.DUPLICATE_ENTRY.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	
	@ExceptionHandler(CustomerNotFoundException.class)
	public final ResponseEntity<Object> handleCustomerNotException(CustomerNotFoundException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(StatusCodes.CUSTOMER_NOT_FOUND.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
	}
	
	
	@ExceptionHandler(MaximumSizeExceededException.class)
	public final ResponseEntity<Object> handleCustomerNotException(MaximumSizeExceededException ex, WebRequest request) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(StatusCodes.MAXIMUM_FAV_ACCOUNTS_EXCEEDED.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(InvalidUserIdExceptionHandler.class)
	public final ResponseEntity<Object> handleIdNotFound(InvalidUserIdExceptionHandler ex) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(StatusCodes.INVALID_ID.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(UnauthorizedException.class)
	public ResponseEntity<ErrorResponse> handleUnauthorizedException(UnauthorizedException details) {
		List<String> listError = new ArrayList<>();
		listError.add(details.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(StatusCodes.UNAUTHORIZED_USER.getErrorCode(), listError);

		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);

	}
	@ExceptionHandler(CustomerNotFound.class)
	public ResponseEntity<ErrorResponse> handleCustomerNotFound(CustomerNotFound details) {
		List<String> listError = new ArrayList<>();
		listError.add(details.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(StatusCodes.CUSTOMER_NOT_FOUND.getErrorCode(), listError);

		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);

	}
	
	@ExceptionHandler(PageLimitExceededException.class)
	public ResponseEntity<ErrorResponse> handlePageLimitExceededException(PageLimitExceededException details) {
		List<String> listError = new ArrayList<>();
		listError.add(details.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(StatusCodes.PAGE_LIMIT_EXCEEDED.getErrorCode(), listError);

		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(InvalidAccountExceptionHandler.class)
	public final ResponseEntity<Object> handleInvalidAccount(InvalidAccountExceptionHandler ex) {
		List<String> details = new ArrayList<>();
		details.add(ex.getLocalizedMessage());
		ErrorResponse error = new ErrorResponse(StatusCodes.INVALID_ACCOUNT_NO.getErrorCode(), details);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
}
