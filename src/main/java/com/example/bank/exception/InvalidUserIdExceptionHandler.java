package com.example.bank.exception;

public class InvalidUserIdExceptionHandler extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidUserIdExceptionHandler(String message) {
		super(message);

	}

}
