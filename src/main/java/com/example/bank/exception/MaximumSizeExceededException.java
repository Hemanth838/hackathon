package com.example.bank.exception;

public class MaximumSizeExceededException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MaximumSizeExceededException(String arg0) {
		super(arg0);
	}
	
	

}
