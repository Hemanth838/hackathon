/**
 * 
 */
package com.example.bank.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.example.bank.config.ConfigurationManager;
import com.example.bank.dto.AccountDto;
import com.example.bank.dto.CustomerDto;
import com.example.bank.dto.FavouriteRecordsResponseDto;
import com.example.bank.entity.Account;
import com.example.bank.entity.Customer;
import com.example.bank.exception.CustomerNotFound;
import com.example.bank.exception.PageLimitExceededException;
import com.example.bank.exception.UnauthorizedException;
import com.example.bank.repository.CustomerRepository;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Service
public class CustomerServiceImpl implements CustomerService {

	Logger logger = LoggerFactory.getLogger(CustomerServiceImpl.class);

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	ConfigurationManager configurationManager;

	/**
	 * This method fetches the favourite accounts from db and converts it to proper response
	 * @param loggedInUser
	 * @param customerId
	 * @param pageNumber
	 * @param pageNumber
	 * @return BookingResponse
	 * @throws CustomerNotFound
	 * @throws UnauthorizedException
	 * @return List<FavouriteRecordsResponseDto>
	 */

	public List<FavouriteRecordsResponseDto> getFavouriteAccounts(Long loggedInUser, Long customerId,
			Integer pageNumber, Integer pageSize) {
		if (loggedInUser != customerId) {
			throw new UnauthorizedException(configurationManager.getUnauthorizedUser());
		}

		Optional<Customer> optionalCustomer = customerRepository.findById(customerId);

		if (!optionalCustomer.isPresent()) {
			throw new CustomerNotFound(configurationManager.getCustomerNotFound());
		}

		Customer customer = optionalCustomer.get();

		Pageable pageable = PageRequest.of(pageNumber, pageSize);

		CustomerDto customerDto = new CustomerDto();
		BeanUtils.copyProperties(customer, customerDto);

		List<Account> accounts = customer.getCustomerAccounts();

		List<AccountDto> dtos = new ArrayList<>();

		accounts.forEach(account -> {
			AccountDto accountDto = new AccountDto();
			BeanUtils.copyProperties(account, accountDto);
			accountDto.setBank(account.getBankId());
			dtos.add(accountDto);

		});

		customerDto.setCustomerAccounts(dtos);

		List<AccountDto> accountsdto = customerDto.getCustomerAccounts();

		List<FavouriteRecordsResponseDto> dtoList = new ArrayList<>();
		accountsdto.forEach(accountDto -> {
			FavouriteRecordsResponseDto favResponseDto = new FavouriteRecordsResponseDto();
			favResponseDto.setAccountOwner(accountDto.getAccountOwner());
			favResponseDto.setIban(accountDto.getIban());
			favResponseDto.setBankName(accountDto.getBank().getBankName());
			dtoList.add(favResponseDto);
		});

		Long start = pageable.getOffset();
		Long end = (start + pageable.getPageSize()) > dtoList.size() ? dtoList.size()
				: (start + pageable.getPageSize());
		
		if(start>end) {
			int pageNum=dtoList.size()/pageSize;
			throw new PageLimitExceededException(configurationManager.getLimitExceeded()+" "+pageNum);
		}
		
		//converting list of objects to pageImpl object for pagination
		return new PageImpl<>(dtoList.subList(start.intValue(), end.intValue()), pageable, dtoList.size()).getContent();
	}

}
