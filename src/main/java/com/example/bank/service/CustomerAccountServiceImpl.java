package com.example.bank.service;

import java.net.URI;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.example.bank.config.ConfigurationManager;
import com.example.bank.constant.Constants;
import com.example.bank.dto.AddAccountRequestDto;
import com.example.bank.dto.BankApiResponseDto;
import com.example.bank.dto.OwnerApiResponseDto;
import com.example.bank.dto.ResponseDto;
import com.example.bank.entity.Account;
import com.example.bank.entity.Bank;
import com.example.bank.entity.Customer;
import com.example.bank.enums.ErrorCodes;
import com.example.bank.enums.StatusCodes;
import com.example.bank.exception.CustomerNotFoundException;
import com.example.bank.exception.DuplicateEntryException;
import com.example.bank.exception.InvalidAccountExceptionHandler;
import com.example.bank.exception.InvalidUserIdExceptionHandler;
import com.example.bank.exception.MaximumSizeExceededException;
import com.example.bank.repository.AccountRepository;
import com.example.bank.repository.BankRepository;
import com.example.bank.repository.CustomerRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * @author bhagyashree.naray
 *
 */
@Service
@Slf4j
@Transactional
public class CustomerAccountServiceImpl implements CustomerAccountService {
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	ConfigurationManager configurationManager;
	@Autowired
	AccountRepository accountRepository;
	
	//@Autowired
	RestTemplate restTemplate = new RestTemplate();

	@Autowired
	ConfigurationManager config;

	@Autowired
	CustomerRepository customerRepo;
	
	@Autowired
	BankRepository bankRepo;
	
	@Autowired
	AccountRepository accoutRepo;

	Logger logger = LoggerFactory.getLogger(CustomerAccountServiceImpl.class);

	/**
	 * @throws InvalidUserIdExceptionHandler
	 * @throws InvalidAccountExceptionHandler
	 * @author rakeshr.ra
	 */
	@Override
	public ResponseDto deleteFavAccount(Long customerId, String iban)
			throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler {

		log.info("CustomerAccountServiceImpl deleteFavAccount :: customer in service layer");
		Optional<Customer> findById = customerRepository.findById(customerId);
		if (!findById.isPresent()) {
			log.error("CustomerAccountServiceImpl deleteFavAccount:: InvalidCustomerIdException occured");
			throw new InvalidUserIdExceptionHandler(configurationManager.getCustomerIdisinvalid());
		}

		Optional<Account> findByIban = accountRepository.findByIban(iban);
		if (!findByIban.isPresent()) {
			log.error("CustomerAccountServiceImpl deleteFavAccount:: Invalid IBAN");
			throw new InvalidAccountExceptionHandler(configurationManager.getAccountNumberNotExist());
		}

		Customer customer = findById.get();
		List<Account> customerAccounts = customer.getCustomerAccounts();
		ResponseDto responseDto = new ResponseDto();

		// to avoid Concurrent modification exception so here we are going
		// with iterator

		Iterator<Account> iterator = customerAccounts.iterator();

		while (iterator.hasNext()) {
			Account temp = iterator.next();

			if (temp.getIban().equals(iban)) {
				iterator.remove();
				responseDto.setMessage(configurationManager.getDeleteSuccess());
				responseDto.setCode(ErrorCodes.DELETE_SUCCESS.getErrorCode());
				log.info("CustomerAccountServiceImpl deleteFavAccount :: iban id deleted");
			}
		}

		if (ErrorCodes.DELETE_SUCCESS.getErrorCode().equals(responseDto.getCode())) {
			customer.setCustomerAccounts(customerAccounts);
			customerRepository.save(customer);

			return responseDto;

		} else {
			log.error("CustomerAccountServiceImpl deleteFavAccount:: account is not in the favourite list");
			throw new InvalidAccountExceptionHandler(configurationManager.getPayeeNotExist());
		}

	}


	
	@Transactional
	public ResponseDto addFavouriteAccount(AddAccountRequestDto request, Long customerId) {
		ResponseEntity<BankApiResponseDto>  bankApiResponseDto ;
		ResponseEntity<OwnerApiResponseDto> ownerApiResponseDto;
		
		logger.info("CustomerAccountServiceImpl: Inside addFavouriteAccount method");
		Optional<Customer> customerOptional = checkForValidCustomer(customerId);
		
		logger.info("Checking repository by customer Id and Iban");
		checkForCustomerAccountExistence(request, customerId);
		
		
		/*fetching bank api details*/
		URI bankApiUrl = UriComponentsBuilder.fromUriString(Constants.BASE_URL)
				.path(Constants.BANK_API_URL)
				.buildAndExpand(Constants.HOSTNAME,request.getIban()).toUri();

		 bankApiResponseDto = restTemplate.getForEntity(bankApiUrl,BankApiResponseDto.class);
		
		
		logger.info("Received response from Bank API : Bank Name: {} ,Bank URL:{}", bankApiResponseDto.getBody().getBankName(),bankApiResponseDto.getBody().getBankAccountServiceURL());
		/*fetching Owner api details*/
		StringBuilder accountsUrl = new StringBuilder(bankApiResponseDto.getBody().getBankAccountServiceURL()).append(Constants.SLASH).append(request.getIban());

		ownerApiResponseDto = restTemplate.getForEntity(accountsUrl.toString(), OwnerApiResponseDto.class);
		
		logger.info("Received response from Owner Name API {} ,{}", ownerApiResponseDto.getBody().getOwnerName(),ownerApiResponseDto.getBody().getIban());

		Account account = createNewOrUseExistingAccount(request.getIban(), bankApiResponseDto.getBody().getBankName(), ownerApiResponseDto.getBody().getOwnerName());
		
		/*Saving customer entity*/
		Customer entity = customerOptional.get();
		List<Account> accList = entity.getCustomerAccounts();
		accList.add(account);
		entity.setCustomerAccounts(accList);
		customerRepo.save(entity);
		
		
		return new ResponseDto(config.getAddAccountSuccessMsg(), StatusCodes.ACCOUNT_ADDED.getErrorCode());
	}

	

	/**Method to check if the account is already added as favorite for the customer
	 * @param request
	 * @param customerId
	 */
	private void checkForCustomerAccountExistence(AddAccountRequestDto request, Long customerId) {
		Customer customer =customerRepo.findByCustomerIdAndCustomerAccountsIban(customerId,request.getIban());
		if(customer!=null){
			logger.error(config.getAccountExistsMsg());
			throw new DuplicateEntryException(config.getAccountExistsMsg());
		}
	}

	/**Method to check if the given Customer Id is valid
	 * @param customerId
	 * @return Optional<Customer>
	 */
	private Optional<Customer> checkForValidCustomer(Long customerId) {
		Optional<Customer> customerOptional = customerRepo.findById(customerId);
		if(!customerOptional.isPresent()){
			logger.error(config.getCustomerNotFoundMsg());
			throw new CustomerNotFoundException(config.getCustomerNotFoundMsg());
		}else{
			Customer cust = customerOptional.get();
			if (cust.getCustomerAccounts().size()>9){
				throw new MaximumSizeExceededException(config.getCustomerNotFoundMsg());
			}
		}
		return customerOptional;
	}

	/**Check if account with the ban already exists,if yes,use that,else create a new one
	 * @param iban
	 * @param bankName
	 * @param ownerName
	 * @return Account
	 */
	private Account createNewOrUseExistingAccount(String iban, String bankName,
			String ownerName) {
		Account account;
		Optional<Account> existingAcc = accoutRepo.findByIban(iban);
			if(existingAcc.isPresent()){
				logger.info("Account already exists,reusing the account");
				account=existingAcc.get();
			}else{
				/*Saving account entity*/
				logger.info("Creating a new account");
				account= new Account();
				account.setAccountOwner(ownerName);
				account.setIban(iban);
				account.setBankId(createNewOrUseExistingBank(bankName));
				accoutRepo.save(account);
			}
		
		return account;
	}
	
	
	private Bank createNewOrUseExistingBank( String bankName) {
		Bank bank;
		Optional<Bank> existingBank = bankRepo.findByBankName(bankName);
			if(existingBank.isPresent()){
				logger.info("Bank already exists,reusing the Bank");
				bank=existingBank.get();
			}else{
				/*Saving account entity*/
				logger.info("Creating a new account");
				bank= new Bank();
				bank.setBankName(bankName);
				bankRepo.save(bank);
			}
		
		return bank;
	}
	

	

	

}
