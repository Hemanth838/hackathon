package com.example.bank.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.bank.config.ConfigurationManager;
import com.example.bank.dto.LoginDto;
import com.example.bank.dto.ResponseDto;
import com.example.bank.entity.Customer;
import com.example.bank.enums.ErrorCodes;
import com.example.bank.exception.InvalidUserIdExceptionHandler;
import com.example.bank.repository.CustomerRepository;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @author rakeshr.ra
 *
 */
@Service
@Slf4j
public class LoginServiceImpl implements LoginService {
	@Autowired
	CustomerRepository customerRepository;
	@Autowired
	ConfigurationManager configurationManager;

	@Override
	public ResponseDto authenticateCustomer(LoginDto loginDto) throws InvalidUserIdExceptionHandler {

		Optional<Customer> findById = customerRepository.findById(loginDto.getCustomerId());
		if (!findById.isPresent()) {

			log.error("LoginServiceImpl authenticateCustomer :: InvalidCustomerIdException occured");
			throw new InvalidUserIdExceptionHandler(configurationManager.getLoginFailed());
		}

		log.info("LoginServiceImpl authenticateCustomer :: customer signed in");

		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage(configurationManager.getLoginSuccess());
		responseDto.setCode(ErrorCodes.LOGIN_SUCCESS.getErrorCode());

		return responseDto;
	}

}
