/**
 * 
 */
package com.example.bank.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.bank.dto.FavouriteRecordsResponseDto;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@Service
public interface CustomerService {
	
	public List<FavouriteRecordsResponseDto> getFavouriteAccounts(Long loggedInUser,Long customerId,Integer pageNumber, Integer pageSize);

	}
