package com.example.bank.service;

import com.example.bank.dto.LoginDto;
import com.example.bank.dto.ResponseDto;
import com.example.bank.exception.InvalidUserIdExceptionHandler;

/**
 * @author rakeshr.ra
 *
 */
public interface LoginService {

	ResponseDto authenticateCustomer(LoginDto loginDto) throws InvalidUserIdExceptionHandler;

}
