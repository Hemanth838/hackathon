package com.example.bank.service;

import com.example.bank.dto.AddAccountRequestDto;
import com.example.bank.dto.ResponseDto;
import com.example.bank.exception.InvalidAccountExceptionHandler;
import com.example.bank.exception.InvalidUserIdExceptionHandler;

import org.springframework.stereotype.Service;

/**
 * @author bhagyashree.naray
 *
 */
@Service

public interface CustomerAccountService {
	
	public ResponseDto addFavouriteAccount(AddAccountRequestDto request, Long customerId);

	public ResponseDto deleteFavAccount(Long customerId, String iban)
			throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler;
	

}
