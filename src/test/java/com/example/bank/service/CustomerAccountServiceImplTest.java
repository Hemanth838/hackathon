package com.example.bank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.example.bank.config.ConfigurationManager;
import com.example.bank.dto.AddAccountRequestDto;
import com.example.bank.dto.BankApiResponseDto;
import com.example.bank.dto.OwnerApiResponseDto;
import com.example.bank.dto.ResponseDto;
import com.example.bank.entity.Account;
import com.example.bank.entity.Bank;
import com.example.bank.entity.Customer;
import com.example.bank.exception.CustomerNotFoundException;
import com.example.bank.exception.DuplicateEntryException;
import com.example.bank.exception.InvalidAccountExceptionHandler;
import com.example.bank.exception.InvalidUserIdExceptionHandler;
import com.example.bank.exception.MaximumSizeExceededException;
import com.example.bank.repository.AccountRepository;
import com.example.bank.repository.BankRepository;
import com.example.bank.repository.CustomerRepository;

@ExtendWith(MockitoExtension.class)
public class CustomerAccountServiceImplTest {

	@InjectMocks
	CustomerAccountServiceImpl customerAccountServiceImpl;

	@Mock
	RestTemplate restTemplate;

	@Mock
	ConfigurationManager configurationManager;

	@Mock
	CustomerRepository customerRepo;

	@Mock
	BankRepository bankRepo;

	@Mock
	AccountRepository accoutRepo;
	
	

	@Test
	public void testAddFavouriteAccountCustomerNotFound() {
		AddAccountRequestDto request = new AddAccountRequestDto();
		request.setIban("abc123");
		Mockito.when(customerRepo.findById(Mockito.anyLong())).thenReturn(Optional.empty());
		Mockito.when(configurationManager.getCustomerNotFoundMsg()).thenReturn(Mockito.anyString());
		assertThrows(CustomerNotFoundException.class, () -> customerAccountServiceImpl.addFavouriteAccount(request, 101l));

	}

	@Test
	public void testAddFavouriteDuplicateEntry() {
		Bank bank = new Bank(1l, "Tokyo Bank");
		List<Account> accounts = new ArrayList<>();
		Account account = new Account(1l, "Bhagya", "abcxyz", bank);
		accounts.add(account);
		Customer customer = new Customer(1l, "abc", "a@gmail", true, accounts);
		AddAccountRequestDto request = new AddAccountRequestDto();
		request.setIban("abc123");
		Mockito.when(customerRepo.findById(Mockito.anyLong())).thenReturn(Optional.of(customer));
		Mockito.when(customerRepo.findByCustomerIdAndCustomerAccountsIban(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(customer);
		Mockito.when(accoutRepo.findByIban(Mockito.anyString())).thenReturn(null);
		assertThrows(DuplicateEntryException.class, () -> customerAccountServiceImpl.addFavouriteAccount(request, 101l));

	}

	@Test
	public void testAddFavouriteEntryExceeds() {
		Bank bank = new Bank(1l, "Tokyo Bank");
		List<Account> accounts = new ArrayList<>();
		Account account = new Account(1l, "Bhagya", "abcxyz", bank);
		Account account1 = new Account(1l, "Bhagya", "abcxyz", bank);
		Account account2 = new Account(1l, "Bhagya", "abcxyz", bank);
		Account account3 = new Account(1l, "Bhagya", "abcxyz", bank);
		Account account4 = new Account(1l, "Bhagya", "abcxyz", bank);
		Account account5 = new Account(1l, "Bhagya", "abcxyz", bank);
		Account account6 = new Account(1l, "Bhagya", "abcxyz", bank);
		Account account7 = new Account(1l, "Bhagya", "abcxyz", bank);
		Account account8 = new Account(1l, "Bhagya", "abcxyz", bank);
		Account account9 = new Account(1l, "Bhagya", "abcxyz", bank);
		Account account10 = new Account(1l, "Bhagya", "abcxyz", bank);

		accounts.add(account);
		accounts.add(account1);
		accounts.add(account2);
		accounts.add(account3);
		accounts.add(account4);
		accounts.add(account5);
		accounts.add(account6);
		accounts.add(account7);
		accounts.add(account8);
		accounts.add(account9);
		accounts.add(account10);

		Customer customer = new Customer(1l, "abc", "a@gmail", true, accounts);
		AddAccountRequestDto request = new AddAccountRequestDto();
		request.setIban("abc123");
		Mockito.when(customerRepo.findById(Mockito.anyLong())).thenReturn(Optional.of(customer));
		Mockito.when(customerRepo.findByCustomerIdAndCustomerAccountsIban(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(customer);
		assertThrows(MaximumSizeExceededException.class, () -> customerAccountServiceImpl.addFavouriteAccount(request, 101l));
	}

	// @Test
	public void testAddFavouriteSuccess() {

		Bank bank = new Bank(1l, "Tokyo Bank");
		List<Account> accounts = new ArrayList<>();
		Account account = new Account(1l, "Bhagya", "abcxyz", bank);
		accounts.add(account);
		Customer customer = new Customer(1l, "abc", "a@gmail", true, accounts);
		AddAccountRequestDto request = new AddAccountRequestDto();
		request.setIban("abc123");
		Mockito.when(customerRepo.findById(Mockito.anyLong())).thenReturn(Optional.of(customer));
		Mockito.when(customerRepo.findByCustomerIdAndCustomerAccountsIban(Mockito.anyLong(), Mockito.anyString()))
				.thenReturn(null);
		Mockito.when(accoutRepo.findByIban(Mockito.anyString())).thenReturn(null);

		BankApiResponseDto dto = new BankApiResponseDto("US Bank", "http://localhost:8040/api/account");

		Mockito.when(restTemplate.getForEntity(Mockito.anyString(), BankApiResponseDto.class))
				.thenReturn(new ResponseEntity<>(dto, HttpStatus.OK));

		OwnerApiResponseDto dto2 = new OwnerApiResponseDto("abc123", "Bhagyasshree M");
		Mockito.when(restTemplate.getForEntity(Mockito.anyString(), OwnerApiResponseDto.class))
				.thenReturn(new ResponseEntity<>(dto2, HttpStatus.OK));

		Mockito.when(bankRepo.save(bank)).thenReturn(bank);

		ResponseDto resDto = customerAccountServiceImpl.addFavouriteAccount(request, 101l);

		assertEquals("Account successfully added to favorites list", resDto.getMessage());

	}

	@Test
	public void testDeleteFavAccount() throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler {
		List<Account> accountList = new ArrayList<>();

		Bank bank = new Bank();
		bank.setBankId(1L);
		bank.setBankName("Denver");
		Account account = new Account();
		account.setAccountId(1L);
		account.setAccountOwner("abc");
		account.setBankId(bank);
		account.setIban("FR33123600033300002");
		accountList.add(account);
		Customer customer = new Customer();
		customer.setCustomerEmail("abc@gmail.com");
		customer.setCustomerId(1L);
		customer.setCustomerName("abc");
		customer.setIsActive(true);
		customer.setCustomerAccounts(accountList);

		Mockito.when(customerRepo.findById(1L)).thenReturn(Optional.of(customer));
		Mockito.when(accoutRepo.findByIban("FR33123600033300002")).thenReturn(Optional.of(account));
		Mockito.when(customerRepo.save(customer)).thenReturn(customer);
		ResponseDto deleteFavAccount = customerAccountServiceImpl.deleteFavAccount(1L, "FR33123600033300002");
		Assertions.assertEquals(configurationManager.getDeleteSuccess(), deleteFavAccount.getMessage());

	}

	/**
	 * 
	 * this test is used for customer trying to re-delete favourite account by
	 * providing valid customerId and valid iban
	 * 
	 * @throws InvalidUserIdExceptionHandler
	 * @throws InvalidAccountExceptionHandler
	 * @author rakeshr.ra
	 */
	@Test
	public void testDeleteFavAccountRedelete() throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler {
		List<Account> accountList = new ArrayList<>();

		Bank bank = new Bank();
		bank.setBankId(1L);
		bank.setBankName("Denver");
		Account account = new Account();
		account.setAccountId(1L);
		account.setAccountOwner("abc");
		account.setBankId(bank);
		account.setIban("FR33123600033300002");
		accountList.add(account);
		Account accounts = new Account();
		accounts.setAccountId(1L);
		accounts.setAccountOwner("abc");
		accounts.setBankId(bank);
		accounts.setIban("FR33123600033300001");

		Customer customer = new Customer();
		customer.setCustomerEmail("abc@gmail.com");
		customer.setCustomerId(1L);
		customer.setCustomerName("abc");
		customer.setIsActive(true);
		customer.setCustomerAccounts(accountList);

		Mockito.when(customerRepo.findById(1L)).thenReturn(Optional.of(customer));
		Mockito.when(accoutRepo.findByIban("FR33123600033300001")).thenReturn(Optional.of(accounts));
		// Mockito.when(customerRepository.save(customer)).thenReturn(customer);

		assertThrows(InvalidAccountExceptionHandler.class,
				() -> customerAccountServiceImpl.deleteFavAccount(1L, "FR33123600033300001"));

	}

	/**
	 * this test is used for customer trying to delete favourite account by
	 * providing valid customerId and invalid iban
	 * 
	 * @throws InvalidUserIdExceptionHandler
	 * @throws InvalidAccountExceptionHandler
	 * @author rakeshr.ra
	 */
	@Test
	public void testDeleteFavAccountInvalidIban() throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler {
		List<Account> accountList = new ArrayList<>();

		Bank bank = new Bank();
		bank.setBankId(1L);
		bank.setBankName("Denver");
		Account account = new Account();
		account.setAccountId(1L);
		account.setAccountOwner("abc");
		account.setBankId(bank);
		account.setIban("FR33123600033300002");
		accountList.add(account);
		Account accounts = new Account();
		accounts.setAccountId(1L);
		accounts.setAccountOwner("abc");
		accounts.setBankId(bank);
		accounts.setIban("FR33123600033300001");
		accountList.add(account);
		accountList.add(accounts);
		Customer customer = new Customer();
		customer.setCustomerEmail("abc@gmail.com");
		customer.setCustomerId(1L);
		customer.setCustomerName("abc");
		customer.setIsActive(true);
		customer.setCustomerAccounts(accountList);

		Mockito.when(customerRepo.findById(1L)).thenReturn(Optional.of(customer));
		Mockito.when(accoutRepo.findByIban("FR33123600033300009")).thenReturn(Optional.ofNullable(null));
		Mockito.when(customerRepo.save(customer)).thenReturn(customer);

		assertThrows(InvalidAccountExceptionHandler.class,
				() -> customerAccountServiceImpl.deleteFavAccount(1L, "FR33123600033300009"));

	}

	/**
	 * this test is used for customer trying to delete favourite account by
	 * providing invalid customerId and valid iban
	 * 
	 * @throws InvalidUserIdExceptionHandler
	 * @throws InvalidAccountExceptionHandler
	 * @author rakeshr.ra
	 */
	@Test
	public void testDeleteFavAccountInvalidCustomerId()
			throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler {
		List<Account> accountList = new ArrayList<>();

		Bank bank = new Bank();
		bank.setBankId(1L);
		bank.setBankName("Denver");
		Account account = new Account();
		account.setAccountId(1L);
		account.setAccountOwner("abc");
		account.setBankId(bank);
		account.setIban("FR33123600033300002");
		accountList.add(account);
		Account accounts = new Account();
		accounts.setAccountId(1L);
		accounts.setAccountOwner("abc");
		accounts.setBankId(bank);
		accounts.setIban("FR33123600033300001");
		accountList.add(account);
		accountList.add(accounts);
		Customer customer = new Customer();
		customer.setCustomerEmail("abc@gmail.com");
		customer.setCustomerId(1L);
		customer.setCustomerName("abc");
		customer.setIsActive(true);
		customer.setCustomerAccounts(accountList);

		Mockito.when(customerRepo.findById(5L)).thenReturn(Optional.ofNullable(null));
		Mockito.when(accoutRepo.findByIban("FR33123600033300009")).thenReturn(Optional.of(accounts));
		Mockito.when(customerRepo.save(customer)).thenReturn(customer);

		assertThrows(InvalidUserIdExceptionHandler.class,
				() -> customerAccountServiceImpl.deleteFavAccount(5L, "FR33123600033300009"));

	}

}
