
/**
 * 
 */
package com.example.bank.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import com.example.bank.config.ConfigurationManager;
import com.example.bank.dto.ResponseDto;
import com.example.bank.entity.Account;
import com.example.bank.entity.Bank;
import com.example.bank.entity.Customer;
import com.example.bank.exception.CustomerNotFound;
import com.example.bank.exception.InvalidAccountExceptionHandler;
import com.example.bank.exception.InvalidUserIdExceptionHandler;
import com.example.bank.exception.PageLimitExceededException;
import com.example.bank.exception.UnauthorizedException;
import com.example.bank.repository.AccountRepository;
import com.example.bank.repository.CustomerRepository;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@ExtendWith(MockitoExtension.class)
public class CustomerServiceImplTest {

	@Mock
	AccountRepository accountRepository;
	@Mock
	CustomerRepository customerRepository;

	@InjectMocks
	CustomerServiceImpl customerServiceImpl;
	
	@InjectMocks
	CustomerAccountServiceImpl customerAccountServiceImpl;

	@Mock
	ConfigurationManager configurationManager;
	
	

	
	
	
	@Test
	public void testDeleteFavAccount() throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler {
		List<Account> accountList = new ArrayList<>();

		Bank bank = new Bank();
		bank.setBankId(1L);
		bank.setBankName("Denver");
		Account account = new Account();
		account.setAccountId(1L);
		account.setAccountOwner("abc");
		account.setBankId(bank);
		account.setIban("FR33123600033300002");
		accountList.add(account);
		Customer customer = new Customer();
		customer.setCustomerEmail("abc@gmail.com");
		customer.setCustomerId(1L);
		customer.setCustomerName("abc");
		customer.setIsActive(true);
		customer.setCustomerAccounts(accountList);

		Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByIban("FR33123600033300002")).thenReturn(Optional.of(account));
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		ResponseDto deleteFavAccount = customerAccountServiceImpl.deleteFavAccount(1L, "FR33123600033300002");
		Assertions.assertEquals(configurationManager.getDeleteSuccess(), deleteFavAccount.getMessage());

	}

	/**
	 * 
	 * this test is used for customer trying to re-delete favourite account by 
	 * providing valid customerId and valid iban
	 * 
	 * @throws InvalidUserIdExceptionHandler
	 * @throws InvalidAccountExceptionHandler
	 * @author rakeshr.ra
	 */
	@Test
	public void testDeleteFavAccountRedelete() throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler {
		List<Account> accountList = new ArrayList<>();

		Bank bank = new Bank();
		bank.setBankId(1L);
		bank.setBankName("Denver");
		Account account = new Account();
		account.setAccountId(1L);
		account.setAccountOwner("abc");
		account.setBankId(bank);
		account.setIban("FR33123600033300002");
		accountList.add(account);
		Account accounts = new Account();
		accounts.setAccountId(1L);
		accounts.setAccountOwner("abc");
		accounts.setBankId(bank);
		accounts.setIban("FR33123600033300001");

		Customer customer = new Customer();
		customer.setCustomerEmail("abc@gmail.com");
		customer.setCustomerId(1L);
		customer.setCustomerName("abc");
		customer.setIsActive(true);
		customer.setCustomerAccounts(accountList);

		Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByIban("FR33123600033300001")).thenReturn(Optional.of(accounts));
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);

		assertThrows(InvalidAccountExceptionHandler.class,
				() -> customerAccountServiceImpl.deleteFavAccount(1L, "FR33123600033300001"));

	}

	/**
	 * this test is used for customer trying to delete favourite account by 
	 * providing valid customerId and  invalid iban
	 * @throws InvalidUserIdExceptionHandler
	 * @throws InvalidAccountExceptionHandler
	 * @author rakeshr.ra
	 */
	@Test
	public void testDeleteFavAccountInvalidIban() throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler {
		List<Account> accountList = new ArrayList<>();

		Bank bank = new Bank();
		bank.setBankId(1L);
		bank.setBankName("Denver");
		Account account = new Account();
		account.setAccountId(1L);
		account.setAccountOwner("abc");
		account.setBankId(bank);
		account.setIban("FR33123600033300002");
		accountList.add(account);
		Account accounts = new Account();
		accounts.setAccountId(1L);
		accounts.setAccountOwner("abc");
		accounts.setBankId(bank);
		accounts.setIban("FR33123600033300001");
		accountList.add(account);
		accountList.add(accounts);
		Customer customer = new Customer();
		customer.setCustomerEmail("abc@gmail.com");
		customer.setCustomerId(1L);
		customer.setCustomerName("abc");
		customer.setIsActive(true);
		customer.setCustomerAccounts(accountList);

		Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));
		Mockito.when(accountRepository.findByIban("FR33123600033300009")).thenReturn(Optional.ofNullable(null));
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);

		assertThrows(InvalidAccountExceptionHandler.class,
				() -> customerAccountServiceImpl.deleteFavAccount(1L, "FR33123600033300009"));

	}
	/**
	 * this test is used for customer trying to delete favourite account by 
	 * providing invalid customerId and  valid iban
	 * @throws InvalidUserIdExceptionHandler
	 * @throws InvalidAccountExceptionHandler
	 * @author rakeshr.ra
	 */
	@Test
	public void testDeleteFavAccountInvalidCustomerId()
			throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler {
		List<Account> accountList = new ArrayList<>();

		Bank bank = new Bank();
		bank.setBankId(1L);
		bank.setBankName("Denver");
		Account account = new Account();
		account.setAccountId(1L);
		account.setAccountOwner("abc");
		account.setBankId(bank);
		account.setIban("FR33123600033300002");
		accountList.add(account);
		Account accounts = new Account();
		accounts.setAccountId(1L);
		accounts.setAccountOwner("abc");
		accounts.setBankId(bank);
		accounts.setIban("FR33123600033300001");
		accountList.add(account);
		accountList.add(accounts);
		Customer customer = new Customer();
		customer.setCustomerEmail("abc@gmail.com");
		customer.setCustomerId(1L);
		customer.setCustomerName("abc");
		customer.setIsActive(true);
		customer.setCustomerAccounts(accountList);

		Mockito.when(customerRepository.findById(5L)).thenReturn(Optional.ofNullable(null));
		Mockito.when(accountRepository.findByIban("FR33123600033300009")).thenReturn(Optional.of(accounts));
		Mockito.when(customerRepository.save(customer)).thenReturn(customer);
		
		assertThrows(InvalidUserIdExceptionHandler.class,
				() -> customerAccountServiceImpl.deleteFavAccount(5L, "FR33123600033300009"));

	}
	
	@Test
	public void TestGetFavouriteAccounts() {

		Bank bank = new Bank(1L, "BNP");

		Account account = new Account(1L, "harry", "US77123500022200002", bank);

		List<Account> accounts = new ArrayList<>();

		accounts.add(account);

		Customer customer = new Customer(1L, "Harry", "harry@gmail.com", true, accounts);

		Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));

		assertEquals(1, customerServiceImpl.getFavouriteAccounts(1L, 1L, 0, 3).size());

	}

	@Test
	public void TestGetFavouriteAccountsForSecondPage() {

		Bank bank = new Bank(1L, "BNP");

		Account account1 = new Account(1L, "harry", "US77123500022200002", bank);
		Account account2 = new Account(2L, "micheal", "US77123500026200002", bank);
		Account account3 = new Account(3L, "john", "US77123500022200004", bank);
		Account account4 = new Account(4L, "keeper", "US77123500022250002", bank);
		Account account5 = new Account(5L, "steve", "US77123500022250002", bank);
		Account account6 = new Account(6L, "david", "US77123500022207002", bank);
		Account account7 = new Account(7L, "jos", "US77123500022200502", bank);

		List<Account> accounts = new ArrayList<>();

		accounts.add(account1);
		accounts.add(account2);
		accounts.add(account3);
		accounts.add(account4);
		accounts.add(account5);
		accounts.add(account6);
		accounts.add(account7);

		Customer customer = new Customer(1L, "Harry", "harry@gmail.com", true, accounts);

		Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));

		assertEquals(3, customerServiceImpl.getFavouriteAccounts(1L, 1L, 1, 3).size());

	}

	@Test
	public void TestGetFavouriteAccountsForPageLimitExceededException() {

		Bank bank = new Bank(1L, "BNP");
		Account account1 = new Account(1L, "harry", "US77123500022200002", bank);
		Account account2 = new Account(2L, "micheal", "US77123500026200002", bank);
		Account account3 = new Account(3L, "john", "US77123500022200004", bank);
		Account account4 = new Account(4L, "keeper", "US77123500022250002", bank);
		Account account5 = new Account(5L, "steve", "US77123500022250002", bank);
		Account account6 = new Account(6L, "david", "US77123500022207002", bank);
		Account account7 = new Account(7L, "jos", "US77123500022200502", bank);

		List<Account> accounts = new ArrayList<>();

		accounts.add(account1);
		accounts.add(account2);
		accounts.add(account3);
		accounts.add(account4);
		accounts.add(account5);
		accounts.add(account6);
		accounts.add(account7);

		Customer customer = new Customer(1L, "Harry", "harry@gmail.com", true, accounts);

		Mockito.when(customerRepository.findById(1L)).thenReturn(Optional.of(customer));

		assertThrows(PageLimitExceededException.class, () -> customerServiceImpl.getFavouriteAccounts(1L, 1L, 3, 3));

	}

	@Test
	public void TestGetFavouriteAccountsForUnauthorizedException() {

		assertThrows(UnauthorizedException.class, () -> customerServiceImpl.getFavouriteAccounts(1L, 2L, 0, 3));

	}

	@Test
	public void TestGetFavouriteAccountsForCustomerNotFound() {

		assertThrows(CustomerNotFound.class, () -> customerServiceImpl.getFavouriteAccounts(1L, 1L, 0, 3));

	}

}
