package com.example.bank.service;

import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.Optional;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.bank.config.ConfigurationManager;
import com.example.bank.dto.LoginDto;
import com.example.bank.dto.ResponseDto;
import com.example.bank.entity.Customer;
import com.example.bank.enums.ErrorCodes;
import com.example.bank.exception.InvalidUserIdExceptionHandler;
import com.example.bank.repository.CustomerRepository;

/**
 * 
 * @author rakeshr.ra
 *
 */
@SpringBootTest
public class LoginServiceImplTest {
	@InjectMocks
	LoginServiceImpl loginServiceImpl;

	@Mock
	CustomerRepository customerRepository;

	@Mock
	ConfigurationManager configurationManager;

	@BeforeEach
	public void init() {

	}

	/**
	 * this test functionality used to testAuthenticateCustomer
	 * 
	 * @throws InvalidUserIdExceptionHandler
	 * @author rakeshr.ra
	 */

	@Test
	public void testAuthenticateCustomer() throws InvalidUserIdExceptionHandler {

		Customer customer = new Customer();
		customer.setCustomerEmail("abc@gmail.com");
		customer.setCustomerId(1L);
		customer.setCustomerName("abc");
		customer.setIsActive(true);

		LoginDto loginDto = new LoginDto();
		loginDto.setCustomerId(1l);
		Mockito.when(customerRepository.findById(loginDto.getCustomerId())).thenReturn(Optional.of(customer));
		ResponseDto authenticateCustomer = loginServiceImpl.authenticateCustomer(loginDto);
		Assertions.assertEquals(ErrorCodes.LOGIN_SUCCESS.getErrorCode(), authenticateCustomer.getCode());

	}

	/**
	 * this test functionality used to testAuthenticateCustomer by providing invalid
	 * customer id
	 * 
	 * @throws InvalidUserIdExceptionHandler
	 * @author rakeshr.ra
	 */

	@Test
	public void testAuthenticateCustomerInvalidIdEx() throws InvalidUserIdExceptionHandler {

		LoginDto loginDto = new LoginDto();
		loginDto.setCustomerId(2L);
		Mockito.when(customerRepository.findById(loginDto.getCustomerId())).thenReturn(Optional.ofNullable(null));

		assertThrows(InvalidUserIdExceptionHandler.class, () -> loginServiceImpl.authenticateCustomer(loginDto));
	}
}
