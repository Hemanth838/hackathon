package com.example.bank.controller;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.bank.config.ConfigurationManager;
import com.example.bank.dto.LoginDto;
import com.example.bank.dto.ResponseDto;
import com.example.bank.enums.ErrorCodes;
import com.example.bank.exception.InvalidUserIdExceptionHandler;
import com.example.bank.service.LoginServiceImpl;

/**
 * 
 * @author rakeshr.ra
 *
 */
@SpringBootTest
public class LoginControllerTest {
	@InjectMocks
	LoginController loginController;
	@Mock
	LoginServiceImpl loginServiceImpl;

	@Test
	public void testAuthenticateCustomer() throws InvalidUserIdExceptionHandler {

		ConfigurationManager configurationManager = new ConfigurationManager();
		LoginDto loginDto = new LoginDto();
		loginDto.setCustomerId(1l);
		ResponseDto responseDto = new ResponseDto();
		responseDto.setMessage(configurationManager.getLoginSuccess());
		responseDto.setCode(ErrorCodes.LOGIN_SUCCESS.getErrorCode());
		Mockito.when(loginServiceImpl.authenticateCustomer(loginDto)).thenReturn(responseDto);
		ResponseEntity<ResponseDto> authenticateCustomer = loginController.authenticateCustomer(loginDto);
		Assertions.assertEquals(HttpStatus.OK, authenticateCustomer.getStatusCode());
	}

}
