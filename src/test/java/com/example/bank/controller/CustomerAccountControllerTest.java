package com.example.bank.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.example.bank.config.ConfigurationManager;
import com.example.bank.dto.AddAccountRequestDto;
import com.example.bank.dto.FavouriteRecordsResponseDto;
import com.example.bank.dto.ResponseDto;
import com.example.bank.enums.StatusCodes;
import com.example.bank.exception.InvalidAccountExceptionHandler;
import com.example.bank.exception.InvalidUserIdExceptionHandler;
import com.example.bank.service.CustomerAccountServiceImpl;
import com.example.bank.service.CustomerServiceImpl;

/**
 * @author hemanth.garlapati
 *
 * 
 */
@ExtendWith(MockitoExtension.class)
public class CustomerAccountControllerTest {
	@Mock
	CustomerServiceImpl customerServiceImpl;

	@InjectMocks
	CustomerAccountController customerAccountController;
	
	@Mock
	CustomerAccountServiceImpl customerAccountServiceImpl;
	
	@Mock
	ConfigurationManager configurationManager;

	

	@Test
	public void testGetFavouriteAccounts()  {
		
		FavouriteRecordsResponseDto dto=new FavouriteRecordsResponseDto("hemanth", "US77123500022200002","BBVA"); 
		
		List<FavouriteRecordsResponseDto> list= new ArrayList<>();
		list.add(dto);

		Mockito.when(customerServiceImpl.getFavouriteAccounts(1L, 1L, 0, 3)).thenReturn(list);
		assertEquals(200, customerAccountController.getFavouriteAccounts(1L, 1L, 0, 3).getStatusCodeValue());
	}
	
	@Test
	public void testAddFavAccount()  {
		


		AddAccountRequestDto request=new AddAccountRequestDto();
		request.setIban("US77123500022200002");
		
		ResponseDto dto=new ResponseDto();
		dto.setCode(200L);
		dto.setMessage("Added Successfully");
		Mockito.when(customerAccountServiceImpl.addFavouriteAccount(request,1L)).thenReturn(dto);
		assertEquals(200, customerAccountController.addFavAccount(1L, request).getStatusCodeValue());
	}
	/**
	 * 
	 * @throws InvalidUserIdExceptionHandler
	 * @throws InvalidAccountExceptionHandler
	 * @author rakeshr.ra
	 */
	

	@Test
	public void deleteFavAccount() throws InvalidUserIdExceptionHandler, InvalidAccountExceptionHandler {
		ConfigurationManager configurationManager = new ConfigurationManager();
		ResponseDto responseDto = new ResponseDto();
		responseDto.setCode(StatusCodes.DELETE_SUCCESS.getErrorCode());
		responseDto.setMessage(configurationManager.getDeleteSuccess());
		Mockito.when(customerAccountServiceImpl.deleteFavAccount(1L, "FR33123600033300002")).thenReturn(responseDto);
		ResponseEntity<ResponseDto> deleteFavAccount = customerAccountController.deleteFavAccount(1L,
				"FR33123600033300002");
		Assertions.assertEquals(HttpStatus.OK, deleteFavAccount.getStatusCode());
	}	

}
